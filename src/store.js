import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './reducers'

function saveToLocalStorage(state) {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch(e) {
        console.warn(e);
    }
}

function loadFromLocalStorage() {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) return undefined;
        return JSON.parse(serializedState);
    } catch(e) {
        console.warn(e);
        return undefined;
    }
}

const persistedState = loadFromLocalStorage();

const store = configureStore({
    reducer: {
        favoriteMovies: rootReducer
    },
    preloadedState: persistedState,
});

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;
