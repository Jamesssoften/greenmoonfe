import { ADD_FAVORITE, REMOVE_FAVORITE } from './actions/actionTypes';

const initialState = {
    favoriteMovies: [],
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FAVORITE:
            return {
                ...state,
                favoriteMovies: [...state.favoriteMovies, action.payload],
            };

        case REMOVE_FAVORITE:
            return {
                ...state,
                favoriteMovies: state.favoriteMovies.filter(
                    (movie) => movie.id !== action.payload
                ),
            };

        default:
            return state;
    }
};

export default rootReducer;
