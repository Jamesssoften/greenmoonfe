import React from 'react';
import MenuComp from "./Menu";
import MovieComponent from "./MovieComponent";

const Homepage = () => {

    return (
        <div>
            <MenuComp />
            <MovieComponent />
        </div>
    )
}

export default Homepage;