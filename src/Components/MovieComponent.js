import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card, CardContent, Typography, Skeleton } from '@mui/material';
import MicIcon from '@mui/icons-material/Mic';
import { Autocomplete, TextField } from '@mui/material';
import { ArrowBack as ArrowBackIcon, Favorite as FavoriteIcon, FavoriteBorder as FavoriteBorderIcon } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { addFavorite, removeFavorite } from '../actions/actions';
import '../App.css';

// --------------------------------------------------------------------------

const MovieComponent = () => {
  const [movies, setMovies] = useState(null);
  const [currentVideo, setCurrentVideo] = useState(null);
  const [searchValue, setSearchValue] = useState("");
  const [selectedMovie, setSelectedMovie] = useState("");
  const dispatch = useDispatch();
  const favouriteMovies = useSelector((state) => state.favoriteMovies.favoriteMovies);
  const [isListening, setIsListening] = useState(false);

// --------------------------------------------------------------------------

  useEffect(() => {
    const getMovies = async () => {
      try {
        const response = await axios.get('https://www.majorcineplex.com/apis/get_movie_avaiable');
        setMovies(response.data.movies);
      } catch (error) {
        console.error('Failed to fetch movies: ', error);
      }
    };

    getMovies();
  }, []);

  const toggleListen = () => {
    setIsListening(!isListening);
    if (!isListening) {
      startListening();
    }
  };

  const startListening = () => {
    if (window.SpeechRecognition || window.webkitSpeechRecognition) {
      const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
      const recognition = new SpeechRecognition();
      recognition.interimResults = true;
      recognition.addEventListener("result", processSpeech);
      recognition.start();

      setTimeout(() => {
        recognition.stop();
        setIsListening(false);
      }, 2500);
    } else {
      console.log("Your browser does not support voice input");
    }
  };

  const processSpeech = (event) => {
    const transcript = Array.from(event.results)
      .map((result) => result[0])
      .map((result) => result.transcript)
      .join("");
    setSearchValue(transcript);
  };

  const handleImageClick = (movie) => {
    setCurrentVideo(movie);
  };

  const handleVideoClose = () => {
    setCurrentVideo(null);
  };

  const handleFavouriteClick = (movie) => {
    dispatch(addFavorite(movie));
  }

  const handleRemoveFavouriteClick = (movie) => {
    dispatch(removeFavorite(movie));
  }

  const filteredMovies = () => {
    if (!movies) {
      return [];
    }
    else if (selectedMovie) {
      return [selectedMovie];
    }
    else if (!searchValue) {
      return movies;
    }
    return movies.filter((movie) => {
      return movie.title_en.toLowerCase().includes(searchValue.toLowerCase()) || movie.title_th.includes(searchValue);
    });
  }

  if (!movies) {
    return (
      <div className='grid-container'>
        {Array.from(new Array(12)).map((_, index) => (
          <Skeleton
            key={index}
            variant="rectangular"
            width={230}
            height={350}
            style={{ borderRadius: '12px' }}
            animation="wave"
          />
        ))}
      </div>
    );
  }

  if (currentVideo) {
    return (
      <div style={{ paddingTop: '50%' }}>
        <ArrowBackIcon style={{
          position: 'absolute', top: '1.5%', left: '4.5%', zIndex: 1000, width: '4%', height: '4%', color: 'white'
        }} onClick={handleVideoClose}></ArrowBackIcon>
        <video style={{ position: 'absolute', top: '0', left: '0', width: '100%', height: '100%' }} controls autoPlay>
          <source src={currentVideo.tr_mp4} type="video/mp4" />
        </video>
        <Card style={{
          color: '#E5E5E5', backgroundColor: '#4F4E71', borderRadius: '12px',
          marginLeft: '10%', marginRight: '10%'
        }} sx={{ minWidth: 275 }} elevation={24}>
          <CardContent>
            <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center' }}>
              <img
                src={currentVideo.poster_url}
                alt={currentVideo.title_en}
                title={`${currentVideo.title_en} / ${currentVideo.title_th}`}
                style={{
                  width: "20%",
                  height: "20%",
                  objectFit: 'cover',
                  borderRadius: '12px',
                  marginRight: '1em',
                  transition: 'transform 0.3s ease-in-out',
                }}
                onMouseOver={e => e.currentTarget.style.transform = 'scale(1.05)'}
                onMouseOut={e => e.currentTarget.style.transform = 'scale(1)'}
              />
              <div style={{ textAlign: 'left', fontFamily: 'Roboto,sans-serif' }}>
                <Typography variant="h4" component="div">{currentVideo.title_th}</Typography><br />
                <Typography sx={{ mb: 1.5 }}>{currentVideo.synopsis_th}</Typography>
                <Typography variant="body2">
                  Actors:
                  {currentVideo.actor.split('/').map((item, key) => {
                    return <span key={key}>{item}<br /></span>
                  })}
                </Typography>
                <Typography variant="body2">
                  Genre:
                  {currentVideo.genre.split('/').map((item, key) => {
                    return <span key={key}>{item}<br /></span>
                  })}
                </Typography>
                <Typography variant="body2" style={{ display: 'flex' }}>Rating:
                  <div style={{
                    border: '1px solid #e3e3e3', marginLeft: '0.8%',
                    paddingLeft: '0.5%',
                    paddingRight: '0.5%'
                  }}>
                    {currentVideo.rating}
                  </div>
                </Typography>
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', position: 'relative' }}>
        <Autocomplete
          options={movies}
          getOptionLabel={(option) => `${option.title_en} / ${option.title_th}`}
          renderInput={(params) => <TextField {...params} label="Search movie" variant="outlined" />}
          style={{ margin: "1em", width: 'calc(100% - 40px)' }} // Adjusted width to account for icon space
          onInputChange={(_, newInputValue) => {
            setSearchValue(newInputValue);
          }}
          onChange={(_, value) => {
            if (value) {
              setSelectedMovie(value);
            } else {
              setSelectedMovie(null);
            }
          }}
        />
        <MicIcon
          style={{
            color: isListening ? 'red' : 'inherit',
            marginRight: '0.50em',
          }}
          onClick={toggleListen}>
          {isListening ? "Stop Voice Search" : "Start Voice Search"}
        </MicIcon>
      </div>
      <div className='grid-container'>
        {filteredMovies().map((movie, index) => (
          <div key={index} style={{ position: 'relative' }}>
            <Typography
              variant='body2'
              style={{
                color: 'white',
                backgroundColor: 'rgba(255, 100, 127, 0.5)',
                backgroundBlendMode: 'color-burn',
                position: 'absolute',
                zIndex: 1,
                top: '85%',
                textAlign: 'center',
                width: '100%',
              }}
            >
              {movie.title_th}
            </Typography>
            {favouriteMovies.find((m) => m.id === movie.id)
              ? <FavoriteIcon
                style={{
                  position: 'absolute',
                  top: '2%',
                  right: '2%',
                  color: '#FF647F',
                  zIndex: 1
                }}
                onClick={() => handleRemoveFavouriteClick(movie.id)}></FavoriteIcon>
              : <FavoriteBorderIcon
                style={{
                  position: 'absolute',
                  top: '2%',
                  right: '2%',
                  color: '#FF647F',
                  zIndex: 1
                }}
                onClick={() => handleFavouriteClick(movie)}></FavoriteBorderIcon>}
            <img
              src={movie.poster_url}
              alt={movie.title_en}
              title={`${movie.title_en} / ${movie.title_th}`}
              style={{
                width: "100%", height: "100%",
                objectFit: 'cover', borderRadius: '12px',
              }}
              onClick={() => handleImageClick(movie)}
              onMouseOver={e => e.currentTarget.style.transform = 'scale(1.02)'}
              onMouseOut={e => e.currentTarget.style.transform = 'scale(1)'}
            />
          </div>
        ))}
      </div>
    </div >
  );
};

export default MovieComponent;
