import * as React from 'react';
import Button from '@mui/material/Button';
import Drawer from '@mui/material/Drawer';
import MenuItem from '@mui/material/MenuItem';
import DehazeRoundedIcon from '@mui/icons-material/DehazeRounded';
import { useNavigate } from 'react-router-dom';

const MenuComp = () => {
    const [open, setOpen] = React.useState(false);
    const navigate = useNavigate();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleLogout = () => {
        navigate('/login');
    };

    const handleFav = () => {
        navigate('/favourite');
    };

    const handleAllMov = () => {
        navigate('/home');
    };

    return (
        <div style={{
            position: 'sticky', top: '0',
            zIndex: '1000', display: 'flex',
            backgroundColor: 'rgba(255, 122, 122, 0.9)',
            marginTop: '-1%',
            paddingTop: '0.2%',
            paddingBottom: '0.2%'
        }}>
            <Button
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClickOpen}
                style={{ color: 'white' }}
            >
                <DehazeRoundedIcon/>
            </Button>
            <Drawer
                id="basic-menu"
                open={open}
                onClose={handleClose}
                sx={{ width: '50%' }}
            >
                <MenuItem onClick={handleAllMov}>All Movies</MenuItem>
                <MenuItem onClick={handleFav}>My Favourite</MenuItem>
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </Drawer>
        </div>
    );
}

export default MenuComp;
