import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Favorite as FavoriteIcon } from '@mui/icons-material';
import { Typography } from '@mui/material';
import { removeFavorite } from '../actions/actions';
import MenuComp from './Menu';

const Favourite = () => {
    const dispatch = useDispatch();
    const favoriteMovies = useSelector((state) => state.favoriteMovies.favoriteMovies);

    const handleRemoveFavouriteClick = (movie) => {
        dispatch(removeFavorite(movie));
    }

    if (favoriteMovies.length === 0) {
        return (
            <div>
                <MenuComp />
                <Typography>No favorite movies selected yet!</Typography>
            </div>
        );
    }

    return (
        <div>
            <MenuComp />
            <div style={{ display: 'grid', gridTemplateColumns: 'repeat(6, 1fr)', gap: '0.5em' }}>
                {favoriteMovies.map((movie, index) => (
                    <div key={index} style={{ position: 'relative' }}>
                        <FavoriteIcon
                            style={{
                                position: 'absolute',
                                top: '2%',
                                right: '2%',
                                color: '#FF647F',
                            }}
                            onClick={() => handleRemoveFavouriteClick(movie.id)}></FavoriteIcon>
                        <img
                            src={movie.poster_url}
                            alt={movie.title_en}
                            title={`${movie.title_en} / ${movie.title_th}`}
                            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '12px' }}
                        />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Favourite;
