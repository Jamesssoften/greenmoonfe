import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Alert, Snackbar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import VdoฺBg from '../VdoBg.mp4';
import '../App.css';

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);
    const navigate = useNavigate();

    const handleUsernameChange = (e) => {
        setUsername(e.target.value);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const validate = (username, password) => {
        return username === "jengaim" && password === "pass1234";
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (validate(username, password)) {
            navigate('/home');
        } else {
            setError(true);
            setTimeout(() => setError(false), 1200);
        }
    };

    return (
        <Box
            sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                height: '100vh',
                width: '100vw',
                position: 'absolute',
                top: 0,
                left: 0,
            }}
        >
            <video
                autoPlay
                loop
                muted
                style={{
                    height: '100%',
                    width: '100%',
                    objectFit: 'cover',
                    filter: 'Blur(3px)',
                }}
            >
                <source
                    src={VdoฺBg}
                    type="video/mp4"
                />
            </video>
            <Box
                sx={{
                    position: 'absolute',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 2,
                    padding: 2,
                    backgroundColor: 'rgba(255,255,255,0.2)',
                    backdropFilter: 'blur(10px)',
                    width: 330,
                    height: 350,
                    transition: 'all 0.3s ease',
                    '&:hover': {
                        backgroundColor: 'rgba(255,255,255,0.35)',
                        transform: 'scale(1.05)',
                    },
                }}
            >
                <div className='login' style={{
                    marginBottom: '10%',
                    marginRight: '35%',
                    color: 'rgb(60, 60, 60)'
                }}>Sign In</div>
                <form onSubmit={handleSubmit}>
                    <Grid container direction="column" spacing={2}>
                        <Grid item>
                            <TextField
                                id="filled"
                                label="Username"
                                variant="filled"
                                value={username}
                                onChange={handleUsernameChange}
                                fullWidth
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                id="filled"
                                label="Password"
                                variant="filled"
                                type="password"
                                value={password}
                                onChange={handlePasswordChange}
                                fullWidth
                            />
                        </Grid>
                        <Grid item>
                            <Button variant="contained" type="submit" style={{ background: 'rgba(200,200,200,0.1)' }}
                                fullWidth>Login</Button>
                        </Grid>
                    </Grid>
                </form>
            </Box>
            {error && (
                <Snackbar open={error} autoHideDuration={6000}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                >
                    <Alert severity="error" sx={{ width: '100%' }}>
                        Invalid Username or Password.
                    </Alert>
                </Snackbar>
            )}
        </Box>
    );
}
export default Login;
