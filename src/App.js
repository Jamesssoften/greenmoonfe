import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom';
import Login from './Components/Login';
import HomePage from './Components/Home';
import Favourite from './Components/Favourite';
import React from 'react';
import { Provider } from 'react-redux';
import store from './store';

const RedirectToLogin = () => {
  const navigate = useNavigate();
  React.useEffect(() => {
    navigate('/login');
  }, [navigate]);

  return null;
};

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<RedirectToLogin />} />
          <Route path="/login" element={<Login />} />
          <Route path="/home" element={<HomePage />} />
          <Route path="/favourite" element={<Favourite />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
